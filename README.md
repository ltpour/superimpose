# superimpose

## Summary

A tool for superimposing images or video frames.

## Usage

python superimpose_images.py output_file input_file_1 input_file_2 ... input_file_n

## A practical example

ls -d ~/Pictures/* | xargs python superimpose_images.py output.png
