import cv2
from os.path import isfile
from sys import argv

def superimpose_image_files(image_files):
    n = len(image_files)
    res = cv2.imread(image_files[0])
    i = 1
    while i < n:
        alpha = 1.0/(i + 1)
        beta = 1.0 - alpha
        img = cv2.imread(image_files[i])
        res_h, res_w = res.shape[:2]
        img_h, img_w = img.shape[:2]
        if res_h >= img_h:
            if res_w >= img_w:
                img = cv2.resize(img, (res_w, res_h), interpolation = cv2.INTER_AREA)
            else:
                img = cv2.resize(img, (img_w, res_h), interpolation = cv2.INTER_AREA)
                res = cv2.resize(img, (img_w, res_h), interpolation = cv2.INTER_AREA)
        else:
            if res_w >= img_w:
                img = cv2.resize(img, (res_w, img_h), interpolation = cv2.INTER_AREA)
                res = cv2.resize(img, (res_w, img_h), interpolation = cv2.INTER_AREA)
            else:
                res = cv2.resize(img, (img_w, img_h), interpolation = cv2.INTER_AREA)
        res = cv2.addWeighted(img, alpha, res, beta, 0.0)
        i += 1        
    return res

def main():
    print(argv)
    if len(argv) < 3:
        print("usage: " + argv[0] + " output_file input_file_1 input_file_2 ... input_file_n")
        return -1
    if isfile(argv[1]):
        print("Output file " + argv[1] + " exists. Not overwriting.")
        return -1
    cv2.imwrite(argv[1], superimpose_image_files(argv[2:]))

if __name__== "__main__":
    main()
