import cv2
from multiprocessing import Process, Pool
from os.path import isfile
from sys import argv

def superimpose_frames(file):
    capture = cv2.VideoCapture(file)
    n = 0
    while capture.isOpened():
        ret, frame = capture.read()
        if ret:
            n += 1
        else:
            break        
    capture.release()
    capture = cv2.VideoCapture(file)
    if capture.isOpened():
        ret, frame = capture.read()
        if ret:
            res = frame
        else:
            return None
    i = 1
    while capture.isOpened():
        ret, frame = capture.read()
        if ret:
            alpha = 1.0/(i + 1)
            beta = 1.0 - alpha
            res = cv2.addWeighted(frame, alpha, res, beta, 0.0)
            i += 1
        else:
            break
    return res

def superimpose_frames_for_videos(videos):
    superimposed_frames = []
    for video in [x for x in videos if x]:
        superimposed_frames.append(superimpose_frames(video))
    return superimposed_frames

def superimpose_superimposed_frames(frames):
    n = len(frames)
    res = frames[0]
    i = 1
    while i < n:
        alpha = 1.0/(i + 1)
        beta = 1.0 - alpha
        img = frames[i]
        res_h, res_w = res.shape[:2]
        img_h, img_w = img.shape[:2]
        if res_h >= img_h:
            if res_w >= img_w:
                img = cv2.resize(img, (res_w, res_h), interpolation = cv2.INTER_AREA)
            else:
                img = cv2.resize(img, (img_w, res_h), interpolation = cv2.INTER_AREA)
                res = cv2.resize(img, (img_w, res_h), interpolation = cv2.INTER_AREA)
        else:
            if res_w >= img_w:
                img = cv2.resize(img, (res_w, img_h), interpolation = cv2.INTER_AREA)
                res = cv2.resize(img, (res_w, img_h), interpolation = cv2.INTER_AREA)
            else:
                res = cv2.resize(img, (img_w, img_h), interpolation = cv2.INTER_AREA)
        res = cv2.addWeighted(img, alpha, res, beta, 0.0)
        i += 1        
    return res

def main():
    if len(argv) < 3:
        print("usage: " + argv[0] + " output_file input_file_1 input_file_2 ... input_file_n")
        return -1
    if isfile(argv[1]):
        print("Output file " + argv[1] + " exists. Not overwriting.")
        return -1
    chunk_count = 4
    video_chunks = [x for x in [argv[2:][i::chunk_count] for i in range(chunk_count)] if x != []]
    pool = Pool(processes=chunk_count)    
    res = [x for xs in pool.map_async(superimpose_frames_for_videos, (video_chunk for video_chunk in video_chunks)).get() for x in xs]
    cv2.imwrite(argv[1], superimpose_superimposed_frames(res))

if __name__== "__main__":
    main()
